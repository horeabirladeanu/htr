from google.cloud import vision
from google.cloud.vision import types


def build_blocks_and_lines(text_annotation):
    lines = get_lines(text_annotation)
    blocks = delimit_blocks(lines)
    # return blocks


def compute_average_diff_lines(lines: dict):
    print(1)
    return (lines[len(lines) - 2]["max_y"] - lines[0]["max_y"]) / (len(lines) - 1)


def delimit_blocks(lines):
    average_difference = compute_average_diff_lines(lines)
    blocks = dict()
    index_block = 0
    blocks[0] = [lines[0]]
    for index_line in range(1, len(lines)):
        last_y = blocks[index_block][len(blocks[index_block]) - 1]["max_y"]
        if lines[index_line]["max_y"] - last_y > average_difference:
            index_block += 1
            blocks[index_block] = [lines[index_line]]
        else:
            blocks[index_block].append(lines[index_line])
    a = blocks
    return blocks


# text_annotation.pages[0].blocks[0].paragraphs[0].words[0].symbols[len(text_annotation.pages[0].blocks[0].paragraphs[0].words[0].symbols)//2]
def compute_min_x(symbol):
    print('1')
    return min(list(map(lambda vertex: vertex.x, symbol.bounding_box.vertices)))


def compute_max_y(symbol):
    print('1')
    return sum(list(map(lambda vertex: vertex.y, symbol.bounding_box.vertices))) / 4


def get_lines(text_annotation: types.AnnotateImageResponse):
    text = text_annotation.text
    # lines = list(filter(lambda x: not x.startswith('/'), list(text.split('\n'))))
    lines = list(text.split('\n'))
    lines_dict = dict()

    first_word_on_every_line = [0]
    for index, line in enumerate(lines):
        lines_dict[index] = dict()
        lines_dict[index]["text"] = line
        words_per_line = line.split(' ')
        first_word_on_every_line.append(first_word_on_every_line[-1] + len(words_per_line))
        lines_dict[index]["words"] = words_per_line

    # first_word_on_every_line.pop(-1)
    current_line = 0
    current_word = 0

    for block in text_annotation.pages[0].blocks:
        for paragraph in block.paragraphs:
            for word in paragraph.words:

                # print('lines_dict[{}][words][{}] = {}; words = {}'
                #       .format(current_line, current_word,
                #               lines_dict[current_line]["words"][current_word],
                #               ''.join(symbol.text for symbol in word.symbols)))

                for symbol in word.symbols:

                    if first_word_on_every_line[current_line] == current_word:
                        lines_dict[current_line]["min_x"] = compute_min_x(symbol)
                        lines_dict[current_line]["max_y"] = compute_max_y(symbol)
                        current_line += 1

                    if symbol.property and symbol.property.detected_break.type:
                        current_word += 1

    # a = text
    # b = lines_dict
    return lines_dict
