import stringdist

def compute_levenshtein_distance(lst: list, word: str) -> list:
    if len(lst) == 0:
        return list(word)

    results = list()
    for elem in lst:
        results.append({'word': elem, 'rank': stringdist.levenshtein(elem, word)})
    return sorted(results, key=lambda aux: aux['rank'])


a = compute_levenshtein_distance(['int', 'char', 'return', 'uuuut'], 'uint')
print(a)
