def process_text_annotation_response(response):

    no_blocks = 0
    blocks = list()
    for block in response.pages[0].blocks:
        no_paragraphs = 0
        paragraphs = list()
        block_text = ''
        for paragraph in block.paragraphs:
            no_words = 0
            words = list()
            paragraph_text = ''
            for word in paragraph.words:
                no_symbols = 0
                symbols = list()
                word_text = ''
                for symbol in word.symbols:
                    symbol_dictionary = dict()
                    symbol_dictionary["detected_break"] = symbol.property.detected_break.type
                    symbol_dictionary["text"] = symbol.text
                    symbol_dictionary["bounding_box"] = symbol.bounding_box.vertices
                    symbols.append(symbol_dictionary)
                    no_symbols += 1
                    word_text += symbol.text

                word_dictionary = dict()
                word_dictionary["symbols"] = symbols
                word_dictionary["no_symbols"] = no_symbols
                word_dictionary["text"] = word_text
                words.append(word_dictionary)
                no_words += 1
                paragraph_text += word_text + ' '

            paragraph_dictionary = dict()
            paragraph_dictionary["words"] = words
            paragraph_dictionary["no_words"] = no_words
            paragraph_dictionary["text"] = paragraph_text
            paragraphs.append(paragraph_dictionary)
            block_text += paragraph_text + '\n'
            no_paragraphs += 1

        block_dictionary = dict()
        block_dictionary["bounding_box"] = block.bounding_box.vertices # 9 linii
        block_dictionary["paragraphs"] = paragraphs
        block_dictionary["no_paragraphs"] = no_paragraphs
        block_dictionary["text"] = block_text
        blocks.append(block_dictionary)
        no_blocks += 1

    return blocks


def write_to_file(blocks):
    # f = open("response.out", "w")
    with open('response.out', 'w', encoding='utf-8') as f:
        f.write(str(len(blocks)) + '\n')
        for block in blocks:
            f.write(str(block["text"]) + '\n')
            f.write(str(block["bounding_box"]) + '\n')
            f.write(str(block["no_paragraphs"]) + '\n')
            for paragraph in block["paragraphs"]:
                f.write(str(paragraph["text"]) + '\n')
                f.write(str(paragraph["no_words"]) + '\n')
                for word in paragraph["words"]:
                    f.write(str(word["text"]) + '\n')
                    f.write(str(word["no_symbols"]) + '\n')
                    for symbol in word["symbols"]:
                        f.write(str(symbol["text"]) + '\n')
                        f.write(str(symbol["detected_break"]) + '\n')
                        f.write(str(symbol["bounding_box"]) + '\n')

def read_from_file(filename):
    with open('response.out', 'r', encoding='utf-8') as f:
        blocks = list()
        no_blocks = int(f.readline().strip())
        for block_index in range(no_blocks):
            block_dictionary = dict()
            block_dictionary["text"] = f.readline().strip()
            vertices = list()
            for i in range(4):
                x = int(f.readline().strip().split(':')[1])
                y = int(f.readline().strip().split(':')[1])
                dictionary_vertices = dict()
                dictionary_vertices["x"] = x
                dictionary_vertices["y"] = y
                vertices.append(dictionary_vertices)
            f.readline()#[
            block_dictionary["bounding_box"] = vertices
            no_paragraphs = int(f.readline().strip())
            block_dictionary["no_paragraphs"] = no_paragraphs
            paragraphs = list()

            for paragraph_index in range(no_paragraphs):
                paragraph_dictionary = dict()
                paragraph_text = f.readline().strip()
                no_words = int(f.readline().strip())
                paragraph_dictionary["text"] = paragraph_text
                paragraph_dictionary["no_words"] = no_words
                words = list()

                for word_index in range(no_words):
                    word_dictionary = dict()
                    word_dictionary["text"] = f.readline().strip()
                    word_dictionary["no_symbols"] = int(f.readline().strip())
                    symbols = list()

                    for symbol_index in range(word_dictionary["no_symbols"]):
                        symbol_dictionary = dict()
                        symbol_dictionary["text"] = f.readline().strip()
                        symbol_dictionary["detected_break"] = int(f.readline().strip())
                        vertices_symbol = list()
                        for i in range(4):
                            x = int(f.readline().strip().split(':')[1])
                            y = int(f.readline().strip().split(':')[1])
                            dictionary_vertices = dict()
                            dictionary_vertices["x"] = x
                            dictionary_vertices["y"] = y
                            vertices_symbol.append(dictionary_vertices)
                        f.readline()
                        symbol_dictionary["bounding_box"] = vertices_symbol
                        symbols.append(symbol_dictionary)

                    word_dictionary["symbols"] = symbols
                    words.append(word_dictionary)
                paragraph_dictionary["words"] = words
                paragraphs.append(paragraph_dictionary)
            block_dictionary["paragraphs"] = paragraphs
            blocks.append(block_dictionary)
        return blocks

