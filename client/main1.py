from api.vision_api import *
from layout.blocks_correction import build_blocks_and_lines
from write_response import write_to_file, process_text_annotation_response, read_from_file

# response = detect_document("../photos/input/image_5.jpg")
# write_to_file(process_text_annotation_response(response))
blocks = read_from_file('response.out')
spaces = 0
breaks_spaces = 0
for block in blocks:
    lista = block["text"].split('^')
    for i in lista:
        spaces += len(list(i.split('@')))

    for paragraph in block["paragraphs"]:
        for word in paragraph["words"]:
            for symbol in word["symbols"]:
                if symbol["detected_break"] in [3, 4, 5]:
                    breaks_spaces += 1
                    print('line: {}; word: {}; symbol: {}; detected break: {}'.format(breaks_spaces, word["text"], symbol["text"], symbol["detected_break"]))

print(spaces)
print(breaks_spaces)
# blocks = build_blocks_and_lines(response)

