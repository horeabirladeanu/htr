from google.cloud import vision
import io
import numpy as np
import cv2
from PIL import Image

def detect_document(path):
    """Detects document features in an image."""

    # preprocess image to have one size of 1024 pixels
    image = Image.open(path)
    image.thumbnail((1024, 1024))
    index = path.rfind('.')
    new_path = path[:index] + '_new.jpg'
    image.save(new_path)
    # image.show()
    print(image.size)

    img = cv2.imread(new_path)
    print(img.shape)
    # img = cv2.imread(new_path)
    # cv2.imshow("cropped", img)
    # row, col = img.shape[:2]
    # print(row)
    # print(col)
    # pts = np.array([[2102, 791], [2149, 789], [2155, 951], [2108, 953]], dtype=np.int32)
    # # pts = np.array([[100, 100], [2100, 100], [120, 200], [90, 1230]], dtype=np.int32)
    # mask = np.zeros((img.shape[0], img.shape[1]))
    # cv2.fillConvexPoly(mask, pts, 1)
    # mask = mask.astype(np.bool)
    #
    # out = np.zeros_like(img)
    # out[mask] = img[mask]
    #
    # cv2.imshow("croppe", out)
    # cv2.waitKey(0)
    cv2.namedWindow("output", cv2.WINDOW_NORMAL)
    client = vision.ImageAnnotatorClient()

    with io.open(new_path, 'rb') as image_file:
        content = image_file.read()

    image = vision.types.Image(content=content)

    response = client.document_text_detection(image=image)
    for page in response.full_text_annotation.pages:
        index_block = 0
        index_symbol = 0
        for block in page.blocks:
            block_path = 'photos/blocks/block_' + str(index) + '.jpeg'
            index_block += 1

            # get the block coordinates by bounding_box
            block_pts = np.array([[block.bounding_box.vertices[0].x, block.bounding_box.vertices[0].y],
                                 [block.bounding_box.vertices[1].x, block.bounding_box.vertices[1].y],
                                 [block.bounding_box.vertices[2].x, block.bounding_box.vertices[2].y],
                                 [block.bounding_box.vertices[3].x, block.bounding_box.vertices[3].y]
                                  ], dtype=np.int32)

            img = cv2.imread(new_path)
            mask = np.zeros((img.shape[0], img.shape[1]))
            cv2.fillConvexPoly(mask, block_pts, 1)
            mask = mask.astype(np.bool)

            out = np.zeros_like(img)
            out[mask] = img[mask]

            cv2.imwrite(block_path, out)
            # print('\nBlock confidence: {}\n'.format(block.confidence))
            all_words_of_paragraph = list()
            for paragraph in block.paragraphs:
                # print('Paragraph confidence: {};'.format(
                #     paragraph.confidence))
                print(all_words_of_paragraph)
                all_words_of_paragraph = list()
                for word in paragraph.words:
                    word_text = ''.join([
                        symbol.text for symbol in word.symbols
                    ])
                    all_words_of_paragraph.append(word_text)
                    # print('Word text: {} (confidence: {}, all: {})'.format(
                    #     word_text, word.confidence, word))

                    for symbol in word.symbols:
                        symbol_path = 'photos/symbols/symbol_' + str(index_symbol) + '.jpeg'
                        index_symbol += 1

                        symbol_pts = np.array([[symbol.bounding_box.vertices[0].x, symbol.bounding_box.vertices[0].y],
                                              [symbol.bounding_box.vertices[1].x, symbol.bounding_box.vertices[1].y],
                                              [symbol.bounding_box.vertices[2].x, symbol.bounding_box.vertices[2].y],
                                              [symbol.bounding_box.vertices[3].x, symbol.bounding_box.vertices[3].y]
                                              ], dtype=np.int32)

                        img = cv2.imread(new_path)
                        mask = np.zeros((img.shape[0], img.shape[1]))
                        cv2.fillConvexPoly(mask, symbol_pts, 1)
                        mask = mask.astype(np.bool)

                        out = np.zeros_like(img)
                        out[mask] = img[mask]
                        cv2.imwrite(symbol_path, out)
                        # print('\tSymbol: {} (confidence: {}, bounding: {}, {} \n)'.format(
                        #     symbol.text, symbol.confidence, symbol, str(symbol)))
    print('\n\n\n\n')
    print(str(response.full_text_annotation))
    if response.error.message:
        raise Exception(
            '{}\nFor more info on error messages, check: '
            'https://cloud.google.com/apis/design/errors'.format(
                response.error.message))
    return response.full_text_annotation
